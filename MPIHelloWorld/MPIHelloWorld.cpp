// MPIHelloWorld.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "mpi.h"
#include <string> 

int main(int *argc, char **argv)
{
	int numTask, rank;
	char message[30];
	MPI_Status status;
	int TAG = 0;
	MPI_Init(argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &numTask);

	if ((rank % 2) == 0)
	{
		if ((rank + 1) != numTask)
		{
			std::cout << "From process - " << rank << "! Total number of process - " << numTask << "\n\n";
			std::string currentMessage = "Message for " + std::to_string(rank + 1) + " Processor";
			strcpy_s(message, currentMessage.c_str());
			MPI_Send(message, 30, MPI_CHAR, rank + 1, TAG, MPI_COMM_WORLD);
		}

	}
	else
	{
		if (rank != 0)
		{
			MPI_Recv(message, 30, MPI_CHAR, rank - 1, TAG, MPI_COMM_WORLD, &status);
		}

		std::cout << "From process - " << rank << "! Total number of process - " << numTask << "\n";
		std::cout << "Received message - " << message << "\n\n";
	}

	MPI_Finalize();
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
